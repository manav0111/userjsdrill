//Q4 Find all users with masters Degree.
const Users = require("./User");

let result = [];

Object.keys(Users).map((key) => {
  //here we are checking the qualification of those who done Master's
  if (Users[key].qualification == "Masters") {
    const obj = { [key]: Users[key] };
    result.push(obj);
  }
});

console.log(result);
