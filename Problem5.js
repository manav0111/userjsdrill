// Q5 Group users based on their Programming language mentioned in their designation

const Users = require("./User");

//difine an object to grouped user on the basis of Language
const groupedLang = {
  Javascript: {},
  Python: {},
  Golang: {},
};

Object.keys(Users).map((key) => {
  //here we are finding the designination of user
  const designation = Users[key].desgination.toLowerCase();

  //checking the language they know and from a group according
  if (designation.includes("golang")) {
    groupedLang.Golang[key] = Users[key];
  } else if (designation.includes("javascript")) {
    groupedLang.Javascript[key] = Users[key];
  } else {
    groupedLang.Python[key] = Users[key];
  }
});

console.log(groupedLang);
