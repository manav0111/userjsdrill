const users = require("./User");

// Q3-Sort users based on their seniority level for Designation - Senior Developer > Developer > Internfor Age - 20 > 10

const sortedUsers = Object.values(users).sort((a, b) => {
  // Define the seniority levels
  const PriorityLevels = {
    "Senior Developer": 3,
    Developer: 2,
    Intern: 1,
  };

  //Compare role wise
  const RoleComparison =
    PriorityLevels[b.desgination] - PriorityLevels[a.desgination];

  // If seniority levels are the same, compare by age
  const ageComparison = b.age - a.age;

  return RoleComparison || ageComparison;
});

console.log(sortedUsers);
